using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public enum BombType
{
    Default, Timer, Infinity, Landmind
}

public class Bomb : MonoBehaviourPunCallbacks, IBomb, IProps
{
    public bool playerOnMe {set; get;}
    public bool playerHasExitMe {set; get;}
    public Tile tileBelow;

    [Header("Bomb Entity")]
    public BombType type;
    public int defautlExplodeTime = 2;
    public Explotion explotion;

    [PunRPC]
    private void RPC_SyncPlayerOnMe(bool newPlayerOnMe) {
        playerOnMe = newPlayerOnMe;
    }

    [PunRPC]
    private void RPC_SyncPlayerHasExitMe(bool newPlayerHasExitMe) {
        playerHasExitMe = newPlayerHasExitMe;
    }

    void OnDestroy() {
        // TODO: detect this (minor), currently is just temp usage
        if (tileBelow) {
            tileBelow.SetAboveMeIsBomb(false);
        }
    }

    void Awake() {
        transform.parent = GameManager.Instance.bombContainer;
    }

    void Start() {
        PlayAudio();

        if (tileBelow) {
            tileBelow.SetAboveMeIsBomb(true);
        }

        StartCoroutine(BombExplodeCounter());
    }

    public IEnumerator BombExplodeCounter() {
        if (type == BombType.Landmind) {
            yield return new WaitUntil(() => playerHasExitMe);
            GetComponent<PhotonView>().RPC("RPC_SyncPlayerHasExitMe", RpcTarget.Others, playerHasExitMe);

            yield return new WaitUntil(() => playerOnMe);
            GetComponent<PhotonView>().RPC("RPC_SyncPlayerOnMe", RpcTarget.Others, playerOnMe);

            PlayLandmindAudio();
        }

        yield return new WaitForSeconds(
                type == BombType.Landmind
                ? defautlExplodeTime - 1.3f
                : defautlExplodeTime);

        Explode();
    }

    public void Explode() {
        Explotion ex = PhotonNetwork.Instantiate("Explotion",
                transform.position,
                Quaternion.identity)
            .GetComponent<Explotion>();
        ex.continueExplode = true;
        ex.explotionType = type;

        Destroy(gameObject);
    }

    public void PlayAudio() {
        GameManager.Instance.gameAudioManager.SpawnBombAudio(this.transform);
    }

    public void PlayLandmindAudio() {
        GameManager.Instance.gameAudioManager.SpawnBombAudio(this.transform);
    }

}
