using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistructableWall : MonoBehaviour, IProps
{
    void Start() {
        transform.parent = GameManager.Instance.distructableWallManager.transform;

        SubscribeEvent();
    }

    /**
     * Check to let tile know it has wall on top then notify it
     */
    void OnTriggerEnter(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsEnterTile(this.transform, tile.id);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsExitTile(this.transform, tile.id);
        }
    }

    public void OnGameTimeout() {
        StartCoroutine(DestroyAnimation());
    }

    /**
     * Subscribe to DistructableWall event
     */
    public void SubscribeEvent() {
        GameEvents.current.onDistructableWallHitExplotion += OnDistructableWallHitExplotion;
        GameEvents.current.onGameTimeout += OnGameTimeout;
    }

    public void OnDistructableWallHitExplotion(Transform t) {
        if (Object.ReferenceEquals(transform, t)) {
            StartCoroutine(DestroyAnimation());
        }
    }

    public IEnumerator DestroyAnimation() {
        //NOTE: this code below is a MUST
        transform.GetComponent<BoxCollider>().size = new Vector3(0, 0, 0);

        // start animation
        yield return new WaitForSeconds(0.1f);

        /// CAUTION: dont let destroy in one frame with animation
        Destroy(gameObject);
    }


    /**
     * IMPORTANT: strictly must unsubscribe any event when obj destoyed
     */
    void OnDestroy() {
        GameEvents.current.onDistructableWallHitExplotion -= OnDistructableWallHitExplotion;
        GameEvents.current.onGameTimeout -= OnGameTimeout;
    }
}
