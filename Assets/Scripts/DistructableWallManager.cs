using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DistructableWallManager : MonoBehaviour
{
    [Header("Objects Props")]
    public GameObject distructableWallPrefab;
    public Transform distructableContainer;

    [Tooltip("Spawn Probability Percentage: Higher Means Bigger Chance to Spawn")]
    [Header("Tiles Configuraton Manager")]
    public int spawnDistructableWallProbability;

    public void Setup(Tiles tiles) {
        GenerateDistructableWall(tiles);
    }

    public void GenerateDistructableWall(Tiles tiles) {
        foreach (Tile tile in tiles) {
            if (!tile.unspwanableDistructableWall
                    && ShouldWallSpawn(tile.dwAdditionalProbability)) {
                SpawnDistructableWall(tile.transform);
            }
        }
    }

    public void SpawnDistructableWall(Transform tile) {
        Vector3 spawnPos = new Vector3(tile.position.x, tile.position.y + 1, tile.position.z);
        GameObject dw = PhotonNetwork.Instantiate(
                "Distructable Wall",
                spawnPos,
                Quaternion.identity);
    }

    /**
     * @param ap: "additional probability" from the tile itself to spawn distructable wall if exist
     */
    public bool ShouldWallSpawn(int ap) {
        if (Random.Range(0, 100) < spawnDistructableWallProbability + ap) {
            return true;
        }
        return false;
    }
}
