using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Explotion : MonoBehaviour, IProps
{
    public bool continueExplode {set; get;}
    public BombType explotionType {set; get;}
    public LayerMask wall;

    /**
     * Check if explode should continue or directly Diminish since explotion is recursively called
     */
    void Start() {
        PlayAudio();

        if (continueExplode) {
            StartCoroutine(ExplodeGenerator());
            StartCoroutine(Diminish());
        } else {
            StartCoroutine(Diminish());
        }
    }

    public IEnumerator ExplodeGenerator() {
        ExplodeBasedOnType();
        yield return null;
    }

    public IEnumerator Diminish() {
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other) {
        EventEnterInvocation(other.GetComponent<IProps>());
    }

    public void EventEnterInvocation(IProps prop) {
        switch(prop) {
            case DistructableWall dw:
                GameEvents.current.DistructableWallHitExplotion(dw.transform);
                break;
            default:
                break;
        }
    }

    /**
     * Define behavior of each explotion based on bomb type
     */
    public void ExplodeBasedOnType() {
        switch (explotionType) {
            case BombType.Default:
                DefaultExplotion();
                break;
            case BombType.Infinity:
                InfinityExplotion();
                break;
            case BombType.Landmind:
                DefaultExplotion();
                break;
            default:
                return;
        }
    }

    /**
     * Infinity Bomb: As long as bomb does not hit wall in next iteration, continue explotion
     */
    public void InfinityExplotion() {
        int r = 1;
        for (bool exAgain = true; exAgain; r++) {
            exAgain = !Physics.CheckSphere(transform.position + new Vector3(r, 0, 0), 0.2f, wall);
            Explode(transform.position + new Vector3(r, 0, 0));
        }

        int l = -1;
        for (bool exAgain = true; exAgain; l--) {
            exAgain = !Physics.CheckSphere(transform.position + new Vector3(l, 0, 0), 0.2f, wall);
            Explode(transform.position + new Vector3(l, 0, 0));
        }

        int t = 1;
        for (bool exAgain = true; exAgain; t++) {
            exAgain = !Physics.CheckSphere(transform.position + new Vector3(0, 0, t), 0.2f, wall);
            Explode(transform.position + new Vector3(0, 0, t));
        }

        int b = -1;
        for (bool exAgain = true; exAgain; b--) {
            exAgain = !Physics.CheckSphere(transform.position + new Vector3(0, 0, b), 0.2f, wall);
            Explode(transform.position + new Vector3(0, 0, b));
        }
    }

    /// SUPER SHITTY WAY BUT IT WORKS!!!
    //  TODO: refactor to be a method check box ahead
    public void DefaultExplotion() {
        Explode(transform.position + new Vector3(1, 0, 0));
        if (!Physics.CheckSphere(transform.position + new Vector3(1, 0, 0), 0.2f, wall)) {
            Explode(transform.position + new Vector3(2, 0, 0));
        }

        Explode(transform.position + new Vector3(-1, 0, 0));
        if (!Physics.CheckSphere(transform.position + new Vector3(-1, 0, 0), 0.2f, wall)) {
            Explode(transform.position + new Vector3(-2, 0, 0));
        }

        Explode(transform.position + new Vector3(0, 0, 1));
        if (!Physics.CheckSphere(transform.position + new Vector3(0, 0, 1), 0.2f, wall)) {
            Explode(transform.position + new Vector3(0, 0, 2));
        }

        Explode(transform.position + new Vector3(0, 0, -1));
        if (!Physics.CheckSphere(transform.position + new Vector3(0, 0, -1), 0.2f, wall)) {
            Explode(transform.position + new Vector3(0, 0, -2));
        }
    }

    /**
     * Explotion factory
     */
    public void Explode(Vector3 pos, bool ce = false) {
        PhotonNetwork.Instantiate(
                "Explotion",
                pos,
                Quaternion.identity)
            .GetComponent<Explotion>()
            .continueExplode = ce;
    }

    public void PlayAudio() {
        GameManager.Instance.gameAudioManager.SpawnExplodeAudio(this.transform);
    }

}
