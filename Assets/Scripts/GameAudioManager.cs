using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudioManager : MonoBehaviour
{
    public AudioSource audioSource;
    public GameObject audioPrefab;

    [Header("Long Background Audio")]
    public AudioClip currentMenuClip;

    [Header("Button Audio")]
    public AudioClip onClickAudio;

    [Header("Player Behavior")]
    public AudioClip onSpawnedBomb;

    [Header("Player Game Event")]
    public AudioClip onPlayerLost;

    [Header("Props Audio")]
    public AudioClip onExplotionAudio;
    public AudioClip onGetQuestionAudio;
    public AudioClip onAnswerTrueQuestionAudio;
    public AudioClip onAnswerFalseQuestionAudio;
    public AudioClip onStompLandmind;

    void Start() {
        audioSource.clip = currentMenuClip;
        audioSource.Play();
    }

    public void SpawnExplodeAudio(Transform parent = null) {
        InstantiateSound(onExplotionAudio, parent);
    }

    public void SpawnBombAudio(Transform parent = null) {
        InstantiateSound(onSpawnedBomb, parent);
    }

    public void SpawnLandmindAudio(Transform parent = null) {
        InstantiateSound(onStompLandmind, parent);
    }

    public void SpawnOnGetQuestionAudio(Transform parent = null) {
        InstantiateSound(onGetQuestionAudio, parent);
    }

    public void SpawnOnAnswerTrueQuestionAudio(Transform parent = null) {
        InstantiateSound(onAnswerTrueQuestionAudio, parent)
            .AddComponent<DestroyOnXSecond>();
    }

    public void SpawnOnAnswerFalseQuestionAudio(Transform parent = null) {
        InstantiateSound(onAnswerFalseQuestionAudio, parent)
            .AddComponent<DestroyOnXSecond>();
    }

    public void SpawnOnPlayerLostAudio(Transform parent = null) {
        InstantiateSound(onPlayerLost, parent)
            .AddComponent<DestroyOnXSecond>().xSecond = 3;
    }

    public GameObject InstantiateSound(AudioClip clip, Transform parent = null) {
        GameObject goAudio = Instantiate(audioPrefab, Vector3.zero, Quaternion.identity, parent);
        AudioSource _audioSource = goAudio.GetComponent<AudioSource>();
        _audioSource.GetComponent<AudioSource>().clip = clip;
        _audioSource.Play();

        return goAudio;
    }
}
