using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Managers")]
    public TileManager tileManager;
    public DistructableWallManager distructableWallManager;
    public QuestionManager questionManager;
    public TimeUIManager timeUIManager;
    public Transform bombContainer;
    public GameAudioManager gameAudioManager;
    public Boomermatter API;

    [Header("Player Setup")]
    public GameObject playerContainer;
    public Transform [] defaultPlayerPos = new Transform[4];

    [Header("Global Props")]
    public int indexDefaultPlayerPos = 0;
    public Camera mainTopCamera;

    [Header("Game Settings")][Tooltip("Per-Minute")]
    public float gameDuration = 3; // per-min
    public PhotonView pv;

    public static GameManager Instance;

    public int numOfPlayerInGame = 0;

    [Header("Player Color")]
    public Material [] mPlayer = new Material[4];

    void Awake() {
        if (Instance != null && Instance != this) {
            Destroy(this.gameObject);
        } else {
            Instance = this;
        }
        StartCoroutine(StartTimeout());
        Setup();
    }

    void Start() {
        SetupPlayer();
    }

    public void IncreaseNumOfPlayerInGame() {
        numOfPlayerInGame+=1;
        pv.RPC("RPC_SyncNumOfPlayerInGame", RpcTarget.Others, numOfPlayerInGame);
    }

    public void DecreaseNumOfPlayerInGame(string notTheWinnerPlayerId) {
        numOfPlayerInGame--;
        pv.RPC("RPC_SyncNumOfPlayerInGame", RpcTarget.Others, numOfPlayerInGame);
        DetectWinner(notTheWinnerPlayerId);
    }

    [PunRPC]
    private void RPC_SyncNumOfPlayerInGame(int newNumOfPlayerIngame) {
        numOfPlayerInGame = newNumOfPlayerIngame;
        DetectWinner();
    }

    public void DetectWinner(string notTheWinnerPlayerId = "") {
        if (PhotonNetwork.PlayerList.Length + numOfPlayerInGame == 1) {
            GameEvents.current.PlayerIsWinning(notTheWinnerPlayerId);
            StartCoroutine(GetEveryoneOutOnX());
        }
    }

    public IEnumerator GetEveryoneOutOnX() {
        yield return new WaitForSeconds(8f);
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("Menu UI");
    }

    public IEnumerator StartTimeout() {
        timeUIManager.SetMinute(gameDuration);
        yield return new WaitForSeconds((gameDuration * 60) + 2);
        GameEvents.current.GameTimeout();
    }

    public void Setup() {
        if (PhotonNetwork.IsMasterClient) {
            SetupGame();
        }
    }

    public void SetupGame() {
        Tiles tiles = tileManager.GetTiles;

        distructableWallManager.Setup(tiles);
        questionManager.Setup(tiles);
    }

    public void SetupPlayer() {
        SpawnNewPlayer();
    }

    public void SpawnNewPlayer() {
        GameObject pc = PhotonNetwork.Instantiate(Path.Combine("Player Container"), Vector3.zero, Quaternion.identity, 0);

        int indexPos = PlayerPrefs.GetInt("DEFAULT_POS") - 1;
        Transform player = pc.transform.GetChild(0);

        Transform defaultPos = GameManager.Instance.defaultPlayerPos[indexPos];
        player.GetComponent<Player>().defaultPos = defaultPos;
        player.position = new Vector3(defaultPos.position.x, 1 ,defaultPos.position.z);
    }

    public void UseGlobalCamera() {
        mainTopCamera.gameObject.SetActive(true);
    }
}
