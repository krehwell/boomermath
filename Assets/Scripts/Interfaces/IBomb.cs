using UnityEngine;

/**
 * Bomb interface
 */
public interface IBomb : IProps
{
    public void Explode();
}
