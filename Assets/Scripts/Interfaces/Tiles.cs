using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boomermath.Utils;

/**
 * List<Tile> Wrapper
 * instead of defining `List<Tile>` every time wants to access tiles,
 * now it simply by using `Tiles tiles`
 */
[System.Serializable]
public class Tiles : IEnumerable<Tile>
{
    public List<Tile> list = new List<Tile>();
    public int Count => list.Count;

    public IEnumerator<Tile> GetEnumerator()
    {
        return list.GetEnumerator();
    }

    // Explicit implementation of non-generic interface
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Add(Tile item)
    {
        list.Add(item);
    }

    public Tile Get(int i) {
        return list[i];
    }

    public void RemoveAt(int i) {
        list.RemoveAt(i);
    }

    public Tile GetTileById(int id) {
        foreach (Tile tile in list) {
            if (tile.id == id) {
                return tile;
            }
        }
        return null;
    }
}

