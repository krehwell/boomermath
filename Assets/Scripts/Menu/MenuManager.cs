using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviourPunCallbacks
{
    [Header("API")]
    public Boomermatter API;

    [Header("Canvas")]
    public GameObject usernameCanvas;
    public GameObject roomCanvas;
    public GameObject menuCanvas;
    public GameObject leaderboardCanvas;

    [Header("UI")]
    public TMP_InputField usernameInput;
    public TMP_InputField roomInput;
    public Button playBtn;

    [Header("Leaderboard Props")]
    public Transform boardContainer;
    public GameObject boardPrefab;

    void Start() {
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("USERNAME", ""))) {
            usernameCanvas.SetActive(true);
        }

        SetupPhoton();
    }

    public void OkBtn() {
        string username = usernameInput.text.Trim();

        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrEmpty(username) || username.Length <= 2) {
            return;
        } else {
            PlayerPrefs.SetString("USERNAME", username);
            usernameCanvas.SetActive(false);
        }
    }

    public void CreateBtn() {
        if (!string.IsNullOrEmpty(roomInput.text)) {
            CreateRoom(roomInput.text);
        }
    }

    public void JoinBtn() {
        if (string.IsNullOrEmpty(roomInput.text)) {
            JoinRandomRoom();
        } else {
            JoinRoomWithId(roomInput.text);
        }
    }

    public void ChangeUsername() {
        usernameInput.text = PlayerPrefs.GetString("USERNAME");
        usernameCanvas.SetActive(true);
    }

    public void EnterRoom() {
        roomCanvas.SetActive(true);
    }

    public void LeaderBoardBtn() {
        foreach (Transform board in boardContainer) {
            if (board) Destroy(board.gameObject);
        }

        leaderboardCanvas.SetActive(true);
        SetLeaderBoard();
    }

    public void SetLeaderBoard() {
        API.CallGetLeaderboard((entry) => {
            foreach (var e in entry.entry) {
                Board board = Instantiate(boardPrefab, boardContainer)
                    .GetComponent<Board>();

                board.SetUsernameField(e.username);
                board.SetWinField(e.win);
            }
        });
    }

    public void BackBtn() {
        if (roomCanvas.activeSelf || leaderboardCanvas.activeSelf) {
            roomCanvas.SetActive(false);
            leaderboardCanvas.SetActive(false);
        }
    }

#region NetworkManager
    public void SetupPhoton() {
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        Debug.Log($"Connected to Server");
        PhotonNetwork.AutomaticallySyncScene = true;
        playBtn.interactable = true;
    }

    public override void OnDisconnected(DisconnectCause cause) {
        Debug.Log($"Cant connect to master because: {cause.ToString()}");
        playBtn.interactable = true;
    }

    public void JoinRandomRoom() {
        PhotonNetwork.JoinRandomRoom();
    }

    public void JoinRoomWithId(string id) {
        PhotonNetwork.JoinRoom(id);
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        Debug.Log($"Cant connect to room because: {message}");
    }

    public void CreateRoom(string id) {
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)4};
        PhotonNetwork.CreateRoom(id, roomOps);
    }

    public override void OnCreateRoomFailed(short returnCode, string message) {
        Debug.Log($"Cant create room because: {message}");
    }

    public override void OnJoinedRoom() {
        SceneManager.LoadScene("Waiting Room");
    }

    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
    }
#endregion
}
