using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class WaitingRoomManager : MonoBehaviourPunCallbacks
{
    public PhotonView thisPhotonView;

    public int currentNumOfPlayers;
    public int maxPlayers = 4;

    public TextMeshProUGUI currentNumOfPlayersUI;
    public TextMeshProUGUI currentWaitingTimeUI;

    public float currentWaitingTime = 30;
    public float waitingStartGameOnFull = 5;

    void Start() {
        PlayerPrefs.DeleteKey("DEFAULT_POS");

        PlayerCountUpdate();
        StartCoroutine(CountdownTime());

        PlayerPrefs.SetInt("DEFAULT_POS", PhotonNetwork.PlayerList.Length);
        PhotonNetwork.NickName = PlayerPrefs.GetString("USERNAME");
    }

    void PlayerCountUpdate() {
        currentNumOfPlayers = PhotonNetwork.PlayerList.Length;
        currentNumOfPlayersUI.text = $"Players: {currentNumOfPlayers}/{maxPlayers}";
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer) {
        PlayerCountUpdate();

        if (PhotonNetwork.IsMasterClient) {
            photonView.RPC("RPC_SendTimer", RpcTarget.Others, currentWaitingTime);
        }
    }

    [PunRPC]
    private void RPC_SendTimer(float timeIn) {
        currentWaitingTime = timeIn;
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player newPlayer) {
        PlayerCountUpdate();
    }

    void Update() {
        currentWaitingTimeUI.text = currentWaitingTime.ToString();
    }

    IEnumerator CountdownTime() {
        yield return new WaitForSeconds(1f);
        currentWaitingTime--;

        if (currentWaitingTime > 0) {
            StartCoroutine(CountdownTime());
        } else {
            ProceedNextScene();
        }
    }

    void ProceedNextScene() {
        if (currentWaitingTime < 1 && currentNumOfPlayers < 2) {
            LeaveRoom();
        } else if (currentWaitingTime < 1 && currentNumOfPlayers > 1) {
            StartGame();
        }
    }

    void StartGame()
    {
        if (!PhotonNetwork.IsMasterClient) {
            return;
        }

        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel("Game");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("Menu UI");
    }
}
