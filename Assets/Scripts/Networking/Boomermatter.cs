using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public delegate void Cb(LeaderboardJSON data);

[System.Serializable]
public class BoomermatterData {
    public string username;
    public int win;
}

[System.Serializable]
public class LeaderboardJSON {
    public BoomermatterData [] entry;
}

public class Boomermatter : MonoBehaviour
{
    public string URI = "https://boomermatter.glitch.me";
    public string APIKEY = "FitAtinJoeSemuaBerjaya";

    public void CallUpdateScore(string username, int score = 1) {
        StartCoroutine(UpdateScore(username, score));
    }

    public void CallGetLeaderboard(Cb callback) {
        StartCoroutine(GetLeaderboard(callback));
    }

    IEnumerator UpdateScore(string username, int score = 1) {
        UnityWebRequest www = UnityWebRequest.Get(URI + $"/rank/update?username={username}&score={score}&apiKey={APIKEY}");
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log($"Can't Update, API ERROR");
        }

        Debug.Log($"{username} Win updated.");
    }

    IEnumerator GetLeaderboard(Cb callback = null) {
        UnityWebRequest www = UnityWebRequest.Get(URI + $"/rank/leaderboard");
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log($"Can't Fetch Leaderboard, API ERROR");
        }

        LeaderboardJSON leaderboardList = new LeaderboardJSON();
        leaderboardList = JsonUtility.FromJson<LeaderboardJSON>(www.downloadHandler.text);

        callback(leaderboardList);
    }
}
