using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : MonoBehaviourPunCallbacks, IProps, IPunObservable
{
    [Header("Player Entity")]
    public string playerId;
    public int playerLife = 3;
    public GameObject playerModel;
    public PhotonView pv;

    [Header("Player Configuration")]
    public Transform model;
    public float playerSpeed = 5f;
    public float playerRotationSpeed = 720f;
    public float playerPointMinDist = 0.05f;
    private Quaternion modelRotationTo;
    private Vector3 modelMovementDirection;

    [Header("Player Transform Relation")]
    public Transform movePoint;
    public Transform playerContainer;
    public LayerMask wall;
    public Tile tileBelow;
    public Transform defaultPos;

    [Header("Player UI Props")]
    public QuestionUIManager questionUIManager;
    public PlayerUIManager playerUIManager;

    [Header("Player Body")]
    public SkinnedMeshRenderer [] bodyParts = new SkinnedMeshRenderer [2];

    public bool playerIsDie = false;
    public bool blinkingAnimationIsPlaying = false;
    public Coroutine playerBlinkingBuffer;
    public bool playerHasWon = false;

#region Getters
    public Tile GetTileBelow => tileBelow;
#endregion

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) { // We own this player: send the others our data
            stream.SendNext(playerId);
            stream.SendNext(playerIsDie);
        } else { // Network player, receive data
            this.playerId = (string)stream.ReceiveNext();
            this.playerIsDie = (bool)stream.ReceiveNext();
        }
    }

    [PunRPC]
    private void RPC_SyncMaterial(int newIndexMat) {
        SyncPlayerMaterial(newIndexMat);
    }

    void Start() {
        if (pv.IsMine) {
            IsMineOnStart();
        }

        movePoint.parent = playerContainer;
    }

    private void IsMineOnStart() {
        playerId = PhotonNetwork.NickName;
        GetComponent<CapsuleCollider>().enabled = true;

        int indexMat = PlayerPrefs.GetInt("DEFAULT_POS") - 1;
        SyncPlayerMaterial(indexMat);
        GetComponent<PhotonView>().RPC("RPC_SyncMaterial", RpcTarget.Others, indexMat);
    }

    public void SyncPlayerMaterial(int indexMat) {
        foreach (SkinnedMeshRenderer skin in bodyParts) {
            skin.material = GameManager.Instance.mPlayer[indexMat];
        }
    }

    void Update() {
        if (pv.IsMine) {
            ModelReDirection();
        }
        DetectPlayerIsDieing();
    }

    void LateUpdate() {
        if (pv.IsMine) {
            Move();
            TriggerPlayerMovingEvent();
        }
    }

    /**
     * Player will always move towards point no metter how
     * Point will only move if it has no wall in front
     */
    public void Move() {
        MovePlayer();

        if (IsPlayerAllowToMove() && !questionUIManager.isActiveAndEnabled) {

            float horizontalAxis;
            float verticalAxis;

#if UNITY_IOS || UNITY_ANDROID
            horizontalAxis = Mathf.Round(SimpleInput.GetAxisRaw("Horizontal"));
            verticalAxis = Mathf.Round(SimpleInput.GetAxisRaw("Vertical"));
#else
            horizontalAxis = Input.GetAxisRaw("Horizontal");
            verticalAxis = Input.GetAxisRaw("Vertical");
#endif

            // don't allow model move diagonally, one input only at a time
            if (horizontalAxis != 0) {
                verticalAxis = 0;
            } else if (verticalAxis != 0){
                horizontalAxis = 0;
            }

            if (IsMovePointAllowToMove(horizontalAxis, verticalAxis)) {
                MoveMovePoint(horizontalAxis, verticalAxis);
                CalculateModelDirection(horizontalAxis, verticalAxis);
            }
        }
    }

    /**
     * Player move towards point
     */
    public void MovePlayer() {
        transform.position = Vector3.MoveTowards(
                transform.position,
                movePoint.position,
                playerSpeed * Time.deltaTime);
    }

    public bool IsPlayerAllowToMove() {
        float distance = Vector3.Distance(transform.position, movePoint.position);

        if (distance <= playerPointMinDist && !playerHasWon) {
            return true;
        }
        return false;
    }

    /**
     * Rotate model according to its direction it towards to based on the CalculateModelDirection()
     */
    public void ModelReDirection() {
        if (modelMovementDirection != Vector3.zero) {
            model.transform.rotation = Quaternion.RotateTowards(
                    model.transform.rotation, modelRotationTo,
                    playerRotationSpeed * Time.deltaTime);
        }
    }

    public void CalculateModelDirection(float hA, float vA) {
        modelMovementDirection = new Vector3(hA, 0, vA);
        modelMovementDirection.Normalize();
        if (modelMovementDirection != Vector3.zero) {
            modelRotationTo = Quaternion.LookRotation(modelMovementDirection, Vector3.up);
        }
    }

    /**
     * "Move Point" move first to be followed by player
     */
    public void MoveMovePoint(float hA, float vA) {
        if (Mathf.Abs(hA) == 1f) {
            movePoint.position += new Vector3(hA, 0f, 0f);
        }

        else if (Mathf.Abs(vA) == 1f) {
            movePoint.position += new Vector3(0f, 0f, vA);
        }
    }

    public bool IsMovePointAllowToMove(float hA, float vA) {
        Vector3 nextHMove = new Vector3 (hA, 0f, 0f);
        Vector3 nextVMove = new Vector3 (0f, 0f, vA);

        bool wallIsAheadH = Physics.CheckSphere(movePoint.position + nextHMove, 0.2f, wall);
        bool wallIsAheadV = Physics.CheckSphere(movePoint.position + nextVMove, 0.2f, wall);

        if (wallIsAheadH || wallIsAheadV) {
            return false;
        }

        return true;
    }

    public void PlayerDie() {
        if (pv.IsMine) {
            playerIsDie = true;

            playerLife--;
            playerUIManager.DecreaseLife();

            if (playerLife > 0) {
                GetComponent<CapsuleCollider>().enabled = false;
                StartCoroutine(SpawnPlayer());
            } else {
                GameManager.Instance.UseGlobalCamera();
                GameManager.Instance.gameAudioManager.SpawnOnPlayerLostAudio();
                GameManager.Instance.DecreaseNumOfPlayerInGame(playerId);
                PhotonNetwork.Destroy(transform.parent.gameObject);
            }
        }
    }

    public void DetectPlayerIsDieing() {
        if (playerIsDie) {
            DiactivePlayerAlpha(true);
        } else {
            if (blinkingAnimationIsPlaying != true) {
                DiactivePlayerAlpha(false);
            }
        }
    }

    public void DiactivePlayerAlpha(bool deactiveate) {
        float alpha = deactiveate ? 0 : 1;

        // set player alpha to 0 so that it is unseen - due to setactive the player introduce bug
        foreach (SkinnedMeshRenderer skin in bodyParts) {
            Color oldColor = skin.material.color;
            Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alpha);
            skin.material.SetColor("_BaseColor", newColor);
        }
    }

    public IEnumerator SpawnPlayer() {
        float playerSafeSpawnTime = 3f;

        Vector3 dp = defaultPos.transform.position;
        movePoint.position = new Vector3(dp.x, movePoint.position.y, dp.z);

        yield return new WaitUntil(() => IsPlayerAllowToMove());
        playerIsDie = false;

        playerBlinkingBuffer = StartCoroutine(PlayerBlinking());
        StartCoroutine(FinishPlayerBlinkingOnX(playerSafeSpawnTime));
    }

    public IEnumerator PlayerBlinking() {
        blinkingAnimationIsPlaying = true;
        float alpha = 1;
        float interval = 0.1f;
        while (true) {
            if (alpha <= 0.3)  {
                interval *= -1;
            } else if (alpha >= 1) {
                interval *= -1;
            }

            foreach (SkinnedMeshRenderer skin in bodyParts) {
                Color oldColor = skin.material.color;
                Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alpha);
                skin.material.SetColor("_BaseColor", newColor);
            }

            alpha = alpha + interval;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public IEnumerator FinishPlayerBlinkingOnX(float freeTime) {
        yield return new WaitForSeconds(freeTime);
        StopCoroutine(playerBlinkingBuffer);
        blinkingAnimationIsPlaying = false;

        int indexMat = PlayerPrefs.GetInt("DEFAULT_POS") - 1;
        SyncPlayerMaterial(indexMat);

        GetComponent<CapsuleCollider>().enabled = true;
    }

    public void SetTileBelowMe(Tile tile) {
        tileBelow = tile;
    }

#region Event Listener
    /**
     * Check player hit with which obj then trigger the event listener so that the particular obj notice
     */
    void OnTriggerEnter(Collider other) {
        EventEnterInvocation(other.GetComponent<IProps>());
    }

    /**
     * @param prop: IProps expected to be either, bomb, tile, or bomb explosion;
     */
    public void EventEnterInvocation(IProps prop) {
        switch(prop) {
            case Tile tile:
                GameEvents.current.PropsEnterTile(this.transform, tile.id);
                SetTileBelowMe(tile);
                break;
            case Question question:
                question.playerOnMyQuestion = this;
                question.questionUIManager = questionUIManager;
                question.DestroyMe();
                break;
            case Bomb bomb:
                bomb.playerOnMe = true;
                break;
            case Explotion explotion:
                PlayerDie();
                GameEvents.current.PlayerHitExplotion(playerId);
                break;
            default:
                break;
        }
    }

    /**
     * Check player has exit then trigger
     */
    void OnTriggerExit(Collider other)
    {
        EventExitInvocation(other.GetComponent<IProps>());
    }

    public void EventExitInvocation(IProps prop) {
        switch(prop) {
            case Tile tile:
                GameEvents.current.PropsExitTile(this.transform, tile.id);
                break;
            case Bomb bomb:
                bomb.playerHasExitMe = true;
                bomb.playerOnMe = false;
                break;
            default:
                break;
        }
    }

    public void TriggerPlayerMovingEvent() {
        float distance = Vector3.Distance(transform.position, movePoint.position);

        if (distance > playerPointMinDist) {
            GameEvents.current.PlayerIsMoving(playerId);
        } else if (distance == 0) {
            GameEvents.current.PlayerStopMoving(playerId);
        }
    }
#endregion
}
