using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerBomb : MonoBehaviour
{
    public Player player;
    public Transform bombContainer;
    // public Bomb bomb;

    [Header("Bomb Collections")]
    public List<Bomb> bombs = new List<Bomb>();

    void Update() {
        if (player.pv.IsMine) {
            if (Input.GetKeyDown("space")) {
                SetBomb();
            }
        }
    }

    public void SetBomb() {
        if (!player.GetTileBelow.aboveMeIsBomb
                && bombs.Count > 0
                && !player.playerIsDie && !player.blinkingAnimationIsPlaying) {
            SpawnBomb();
        }
    }

    public void SpawnBomb() {
        Tile tb = player.GetTileBelow;
        Vector3 bombPos = tb.GetPosition + new Vector3(0f, 1f, 0f);

        string bombName;
        switch (bombs[0].type) {
            case BombType.Infinity:
                bombName = "Bomb Infinity";
                break;
            case BombType.Landmind:
                bombName = "Bomb Landmind";
                break;
            default:
                bombName = "Bomb Default";
                break;
        }

        PhotonNetwork.Instantiate(bombName, bombPos, Quaternion.identity)
            .GetComponent<Bomb>().tileBelow = player.GetTileBelow;

        PostSpawnBomb();
    }

    public void PostSpawnBomb() {
        player.playerUIManager.RemoveBomb();
        bombs.RemoveAt(0);
    }
}
