using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    public Player player;
    public Transform playerTransform;
    public Transform playerContainer;
    private Vector3 camOffset;

    [Header("Clamp Camera")]
    public bool clampCamera;
    public float maxClampZ = -2f;
    public float minClampZ = -5.3f;

    [Range(0.01f, 3.0f)]
    public float smoothFactor = 0.2f;

    void Start() {
        if (player.pv.IsMine) {
            GetComponent<Camera>().enabled = true;
        }
        camOffset = transform.position - playerTransform.transform.position;
        transform.parent = playerContainer;
    }

    void LateUpdate() {
        Vector3 newPos = playerTransform.position + camOffset;

        if (clampCamera) {
            if (newPos.z > maxClampZ) {
                newPos.z = maxClampZ;
            }

            if (newPos.z < minClampZ) {
                newPos.z = minClampZ;
            }
        }

        transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);
    }
}
