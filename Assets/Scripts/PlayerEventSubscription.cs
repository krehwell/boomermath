using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEventSubscription : MonoBehaviour
{
    public Player player;
    public Animator playerAnimator;
    public bool playerismoving = false;
    public AudioSource audioSource;

    void Start() {
        SubscribeEvent();
    }

    /**
     * Subscribe to player movement event
     */
    public void SubscribeEvent() {
        GameEvents.current.onPlayerIsMoving += OnPlayerIsMoving;
        GameEvents.current.onPlayerStopMoving += OnPlayerStopMoving;
        GameEvents.current.onPlayerHitExplotion += OnPlayerHitExpotion;
        GameEvents.current.onPlayerIsWinning += OnPlayerIsWinning;
    }

    public void OnPlayerIsMoving(string playerId) {
        if (!playerismoving && player.pv.IsMine) {
            playerAnimator.SetBool("isRun", true);
        }
        playerismoving = true;
    }

    public void OnPlayerStopMoving(string playerId) {
        if (playerismoving && player.pv.IsMine) {
            playerAnimator.SetBool("isRun", false);
        }
        playerismoving = false;
    }

    public void OnPlayerHitExpotion(string playerId) {
        playerAnimator.SetBool("isRun", false);
    }

    public void OnPlayerIsWinning(string notTheWinnerPlayerId) {
        if (player.pv.IsMine) {
            playerAnimator.SetBool("isRun", true);
            player.playerHasWon = true;
            StartCoroutine(PlayerWinAnimation());

            if (player.playerId != notTheWinnerPlayerId) {
                GameManager.Instance.API.CallUpdateScore(player.playerId);
            }
        }
    }

    IEnumerator PlayerWinAnimation() {
        StartCoroutine(PlayerRotateHappily());
        StartCoroutine(PlayerJumpHappily());
        yield return new WaitForSeconds(7f);
    }

    IEnumerator PlayerRotateHappily() {
        int yVal = 0;
        while (true) {
            yield return new WaitForSeconds(0.00005f);
            transform.rotation = Quaternion.Euler(0, yVal++, 0);
        }
    }

    IEnumerator PlayerJumpHappily() {
        float yVal = 0f;
        float interval = 0.1f;
        while (true) {
            if (yVal >= 1.5) {
                interval = -0.1f;
            } else if (yVal <= 0) {
                interval = 0.1f;
            }

            yield return new WaitForSeconds(0.02f);
            transform.position = new Vector3
                (player.movePoint.position.x, yVal+=interval, player.movePoint.position.z);
        }
    }

    /**
     * IMPORTANT: strictly must unsubscribe any event when obj destoyed
     */
    void OnDestroy() {
        GameEvents.current.onPlayerIsMoving -= OnPlayerIsMoving;
        GameEvents.current.onPlayerStopMoving -= OnPlayerStopMoving;
        GameEvents.current.onPlayerHitExplotion -= OnPlayerHitExpotion;
        GameEvents.current.onPlayerIsWinning -= OnPlayerIsWinning;
    }
}
