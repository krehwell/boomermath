using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public enum QuestionLevel {
    EASY, MEDIUM, HARD
}

public enum QuestionType {
    ADDITION, SUBSTRACTION, MULTIPLICATION, DIVISION
}

public class Question: MonoBehaviour, IProps
{
    public QuestionManager questionManager;
    public QuestionUIManager questionUIManager;

    public float limitTimeToAnswer = 7f;

    public int lhs;
    public int rhs = 0;
    public float answer;
    public float choiceA;
    public float choiceB;
    public float choiceC;
    public float choiceD;
    public string questionTypeString;

    public QuestionLevel difficulty;
    public QuestionType questionType;
    public int qMax;
    public int qMin;

    public Bomb qBomb {set; get;}
    public Bomb [] bombOptions = new Bomb[3];

    public Player playerOnMyQuestion;

    public bool hasAnswered = false;

    void Awake() {
        SetupQuestionProps();
        GenerateQuestion();
    }

    [PunRPC]
    private void RPC_SendHasAnswered(bool newHasAnswered) {
        hasAnswered = newHasAnswered;
    }

    void SetupQuestionProps() {
        questionManager = GameManager.Instance.questionManager;
        transform.parent = questionManager.transform;
        difficulty = (QuestionLevel)Random.Range(0, 3);
        questionType = (QuestionType)Random.Range(0, 4);
        qBomb = bombOptions[Random.Range(0, bombOptions.Length)];
    }

    void Start() {
        questionManager.IncNumOfQuestionOnTiles();
    }

    void Update() {
        if (hasAnswered) {
            StopAllCoroutines();
            Destroy(gameObject);
        }
    }

    void OnDestroy() {
        if (questionUIManager){
            questionUIManager.gameObject.SetActive(false);
        }
        questionManager.DecNumOfQuestionOnTiles();
    }

    public void DestroyMe() {
        // don't allow other player to collect same question
        transform.GetComponent<BoxCollider>().enabled = false;

        PopUpQuestion();
        StartCoroutine(DestroyOnXSec(limitTimeToAnswer));
    }

    public IEnumerator DestroyOnXSec(float sec) {
        yield return new WaitForSeconds(sec);
        questionUIManager.SetNullAllListener();

        hasAnswered = true;
    }

    /**
     * Check to let tile know it has wall on top then notify it
     */
    void OnTriggerEnter(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsEnterTile(this.transform, tile.id);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsExitTile(this.transform, tile.id);
        }
    }

    public void GenerateQuestion() {
        switch (difficulty) {
            case QuestionLevel.EASY:
                GenerateMinMax(0, 20);
                break;
            case QuestionLevel.MEDIUM:
                GenerateMinMax(0, 60);
                break;
            case QuestionLevel.HARD:
                GenerateMinMax(0, 80);
                break;
            default:
                break;
        }
    }

    public void GenerateMinMax(int min, int max) {
        switch (questionType) {
            case QuestionType.ADDITION:
                questionTypeString = "+";
                GenerateAnswers(min, max + 20);
                break;
            case QuestionType.SUBSTRACTION:
                questionTypeString = "-";
                GenerateAnswers(min, max + 20);
                break;
            case QuestionType.MULTIPLICATION:
                questionTypeString = "*";
                GenerateAnswers(min, max / 2);
                break;
            case QuestionType.DIVISION:
                questionTypeString = "/";
                GenerateAnswers(min, max);
                break;
            default:
                break;
        }
    }

    public void GenerateAnswers(int min, int max) {
        switch (questionType) {
            case QuestionType.ADDITION:
                lhs = Random.Range(min, max);
                rhs = Random.Range(min, max);
                answer = lhs + rhs;
                break;
            case QuestionType.SUBSTRACTION:
                lhs = Random.Range(min, max);
                rhs = Random.Range(min, max);
                answer = lhs - rhs;
                break;
            case QuestionType.MULTIPLICATION:
                lhs = Random.Range(min+1, max);
                rhs = Random.Range(min+1, max);
                answer = lhs * rhs;
                break;
            case QuestionType.DIVISION:
                lhs = Random.Range(min+5, max);
                while (rhs > lhs || rhs <= 0) {
                    rhs = Random.Range(min+1, max);
                }
                answer = (float)lhs / (float)rhs;
                break;
            default:
                break;
        }

        GenerateChoices(answer);
    }

    /**
     * At this point the choices generated from Randomizer will always propbably in this form
     * a: least,
     * b: second least
     * answer: answer (correct)
     * c: second highest
     * d: highest
     */
    public void GenerateChoices(float ans) {
        float distOff = // distraction offset
            difficulty == QuestionLevel.EASY ? 20 :
            difficulty == QuestionLevel.MEDIUM ? 10 :
            difficulty == QuestionLevel.HARD ? 5 : 7; // incase things happened just fallback to 7

        switch (questionType) {
            case QuestionType.DIVISION:
                choiceA = Random.Range(ans - distOff, ans);
                choiceB = Random.Range((ans - distOff) / 2, ans);
                choiceC = Random.Range(ans, (ans + distOff) / 2);
                choiceD = Random.Range(ans, (ans + distOff));
                break;
            default:
                choiceA = (int)Random.Range(ans - distOff, ans);
                choiceB = (int)Random.Range((ans - distOff) / 2, ans);
                choiceC = (int)Random.Range(ans, (ans + distOff) / 2);
                choiceD = (int)Random.Range(ans, (ans + distOff));
                break;
        }

        /**
         * Make sure there will be no same choices
         */
        int iterationLoop = 0;
        for(bool keepRandomize = true; keepRandomize && iterationLoop < 20; iterationLoop++) {
            keepRandomize = false;

            if (choiceA == answer) {
                choiceA = (int)Random.Range(ans - distOff, ans + iterationLoop);
                keepRandomize = true;
            } if (choiceB == answer) {
                choiceB = (int)Random.Range((ans - distOff) / 2, ans + iterationLoop);
                keepRandomize = true;
            } if (choiceC == answer) {
                choiceC = (int)Random.Range(ans, ((ans + distOff) / 2) + iterationLoop);
                keepRandomize = true;
            } if (choiceD == answer) {
                choiceD = (int)Random.Range(ans, (ans + distOff) + iterationLoop);
                keepRandomize = true;
            }
        }
    }

    public void PopUpQuestion() {
        // Debug.Log($"Question: {lhs} {questionType} {rhs}\nA: {choiceA}\nB: {choiceB}\nanswer: {answer}\nC: {choiceC}\nD: {choiceD}\nDiff: {difficulty}");
        GameManager.Instance.gameAudioManager.SpawnOnGetQuestionAudio(this.transform);

        if (playerOnMyQuestion.pv.IsMine) {
            questionUIManager.gameObject.SetActive(true);

            questionUIManager.SetupUIManager(lhs, rhs, questionTypeString, answer,
                    choiceA, choiceB, choiceC, choiceD,
                    PlayerAnswerTrue, PlayerAnswerFalse);
        }
    }

    public void PlayerAnswerTrue() {
        Debug.Log("Correct");
        GameManager.Instance.gameAudioManager.SpawnOnAnswerTrueQuestionAudio();

        playerOnMyQuestion.GetComponent<PlayerBomb>().bombs.Add(qBomb);
        playerOnMyQuestion.playerUIManager.AddBomb(qBomb.type);

        if (questionUIManager) {
            questionUIManager.SetNullAllListener();
            questionUIManager.gameObject.SetActive(false);
        }
        StopAllCoroutines();
        SetHasAnsweredToTrue();
    }

    public void PlayerAnswerFalse() {
        Debug.Log("Incorrect");
        GameManager.Instance.gameAudioManager.SpawnOnAnswerFalseQuestionAudio();

        if (questionUIManager) {
            questionUIManager.SetNullAllListener();
            questionUIManager.gameObject.SetActive(false);
        }
        StopAllCoroutines();
        SetHasAnsweredToTrue();
    }

    /**
     * Due to syncing problem, destoy quetsion must be done in this way
     */
    public void SetHasAnsweredToTrue() {
        hasAnswered = true;
        if (GetComponent<PhotonView>()) {
            GetComponent<PhotonView>().RPC("RPC_SendHasAnswered", RpcTarget.Others, true);
        }
    }
}
