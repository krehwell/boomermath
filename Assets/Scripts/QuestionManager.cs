using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class QuestionManager : MonoBehaviour
{
    [Header("Question Entity")]
    public float minQuestonSpawnTimerate = 4;
    public float maxQuestonSpawnTimerate = 8;

    // TODO: make an rpc to keep track of this value
    [Range(1, 50)]
    public int maxNumOfQuestionOnTiles;
    public int currentNumOfQuestionOnTiles;

    [Header("Question Props")]
    public GameObject questionPrefab;
    public Transform questionContainer;
    public QuestionUIManager questionUIManager;

    [Header("Default Initial Tile Has Question")]
    public Transform [] initialSpawnedQuestionTile = new Transform[4];

    public void Setup(Tiles tiles) {
        StartCoroutine(SpawnQuestionCounter(tiles));

        // Directly spawn questions on these tiles
        foreach (Transform tile in initialSpawnedQuestionTile) {
            SpawnQuestion(tile);
        }
    }

    IEnumerator Start() {
        yield return new WaitForSeconds(0.2f);
        SubscribeEvent();
    }

    public void OnGameTimeout() {
        maxNumOfQuestionOnTiles = 50;
        minQuestonSpawnTimerate = 0.5f;
        maxQuestonSpawnTimerate = 3.5f;
    }

    /**
     * Counter/Timer for question to spawn
     */
    public IEnumerator SpawnQuestionCounter(Tiles tiles) {
        DynamicQuestionSpawnTime();

        yield return new WaitForSeconds(
                Random.Range(minQuestonSpawnTimerate, maxQuestonSpawnTimerate));

        if (AllowQuestionToSpawn()) {
            Tile choosenTile = GetRandomAboveIsEmptyTile(tiles);
            SpawnQuestion(choosenTile.transform);
        }

        StartCoroutine(SpawnQuestionCounter(tiles));
    }

    public void DynamicQuestionSpawnTime() {
        if (currentNumOfQuestionOnTiles < 10) {
            minQuestonSpawnTimerate = 0.5f;
            maxQuestonSpawnTimerate = 1.5f;
        } else if (currentNumOfQuestionOnTiles < 15) {
            minQuestonSpawnTimerate = 1f;
            maxQuestonSpawnTimerate = 2f;
        } else if (currentNumOfQuestionOnTiles < 20 ) {
            minQuestonSpawnTimerate = 1f;
            maxQuestonSpawnTimerate = 4f;
        } else if (currentNumOfQuestionOnTiles < 30 ) {
            minQuestonSpawnTimerate = 2f;
            maxQuestonSpawnTimerate = 6f;
        } else if (currentNumOfQuestionOnTiles < 40 ) {
            minQuestonSpawnTimerate = 4f;
            maxQuestonSpawnTimerate = 8f;
        }
    }

    public void SpawnQuestion(Transform tile) {
        Vector3 pos = new Vector3(tile.position.x, 1, tile.position.z);
        GameObject newQueston = PhotonNetwork.Instantiate(
                "Question Box",
                pos,
                Quaternion.identity);
    }

    /**
     * Get the empty tile which has nothing above it
     * @return Tile: empty tile choosen
     */
    public Tile GetRandomAboveIsEmptyTile(Tiles tiles) {
        List<int> indexOfEmptyTiles = new List<int>();
        foreach (Tile tile in tiles) {
            if (tile.aboveIsEmpty && !tile.aboveMeIsBomb) {
                indexOfEmptyTiles.Add(tile.id);
            }
        }

        int choosenEmptyTile = indexOfEmptyTiles[
            Random.Range(0, indexOfEmptyTiles.Count)
        ];
        return tiles.GetTileById(choosenEmptyTile);
    }

    public bool AllowQuestionToSpawn() {
        int mq = maxNumOfQuestionOnTiles;
        int cq = currentNumOfQuestionOnTiles;
        return cq < mq;
    }

#region Setters
    public void DecNumOfQuestionOnTiles(){currentNumOfQuestionOnTiles--;}
    public void IncNumOfQuestionOnTiles(){currentNumOfQuestionOnTiles++;}
#endregion

    public void SubscribeEvent() {
        GameEvents.current.onGameTimeout += OnGameTimeout;
    }

    public void UnSubscribeEvent() {
        GameEvents.current.onGameTimeout -= OnGameTimeout;
    }
}
