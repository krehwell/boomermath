using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour, IProps
{
    public IProps itemAboveMe;

    [Header("Tile Entity")]
    public bool aboveIsEmpty = true;
    public bool aboveMeIsBomb = false;
    public int id;

    [Header("Tile Properties")]
    public bool unspwanableDistructableWall;
    public int dwAdditionalProbability = 0;

#region Getters
    public Vector3 GetPosition => transform.position;
#endregion

    void Start() {
        SubscribeEvent();
    }

    /**
     * Subscribe to each event to monitor all behavior from each props above me
     */
    public void SubscribeEvent() {
        GameEvents.current.onPropsEnterTile += OnPropsEnterTile;
        GameEvents.current.onPropsExitTile += OnPropsExitTile;
    }

    /**
     * Doesn't matter what obj, anything that come above me will set aboveIsEmpty = true
     * @param objAboveMe: the particular obj that triggers me
     * @param _id: id of the tile the obj actually trigger
     */
    public void OnPropsEnterTile(Transform objAboveMe, int _id) {
        if (id == _id) {
            SetAboveMeIsEmpty(false);
        }
    }

    /**
     * @param objAboveMe: the particular obj that triggers me
     * @param _id: id of the tile the obj actually trigger
     */
    public void OnPropsExitTile(Transform objAboveMe, int _id) {
        if (id == _id) {
            SetAboveMeIsEmpty(true);
        }
    }

#region Setters
    public void SetAboveMeIsEmpty(bool b) {
        aboveIsEmpty = b;
    }

    public void SetItemAboveMe(IProps obj) {
        itemAboveMe = obj;
    }

    public void SetAboveMeIsBomb(bool b) {
        aboveMeIsBomb = b;
    }
#endregion

    /**
     * IMPORTANT: strictly must unsubscribe any event when obj destroyed
     */
    void OnDestroy() {
        GameEvents.current.onPropsEnterTile -= OnPropsEnterTile;
        GameEvents.current.onPropsExitTile -= OnPropsExitTile;
    }
}
