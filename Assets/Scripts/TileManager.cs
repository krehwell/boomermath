using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    [Header("Tiles Props")]
    public Tiles tiles;

#region Getters
    public Tiles GetTiles => tiles;
#endregion

    /**
     * Used only on game not played to set all the tiles(child) in list
     */
    [ContextMenu("Set All Tiles to List")]
    public void SetAllTilesToList() {
        foreach (Transform child in transform) {
            Tile t = child.GetComponent<Tile>();
            tiles.Add(t);
        }
    }
}
