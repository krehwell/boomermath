using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
    public TMP_InputField usernameField;
    public TMP_InputField winField;

    public void SetUsernameField(string username) {
        usernameField.text = username;
    }

    public void SetWinField(int win) {
        winField.text = win.ToString();
    }
}
