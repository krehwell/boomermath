using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIManager : MonoBehaviour
{
    [Header("Self - Player")]
    public Player player;
    public PlayerBomb playerBomb;

    [Header("Container")]
    public RectTransform healthContainer;
    public RectTransform bombContainer;

    [Header("Prefabs")]
    public GameObject bombDefaultPrefab;
    public GameObject bombInfinityPrefab;
    public GameObject bombLandmindPrefab;

    void Start() {
        if (!player.pv.IsMine) {
            gameObject.SetActive(false);
        }
    }

    public void DecreaseLife() {
        Destroy(healthContainer.GetChild(0).gameObject);
    }

    public void AddBomb(BombType bombType) {
        switch (bombType) {
            case BombType.Default:
                Instantiate(bombDefaultPrefab, bombContainer);
                break;
            case BombType.Infinity:
                Instantiate(bombInfinityPrefab, bombContainer);
                break;
            case BombType.Landmind:
                Instantiate(bombLandmindPrefab, bombContainer);
                break;
        }
    }

    public void RemoveBomb() {
        Destroy(bombContainer.GetChild(0).gameObject);
    }
}
