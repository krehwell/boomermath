using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Boomermath.Utils;

public class QuestionUIManager : MonoBehaviour
{
    public delegate void VoidCb();
    public TextMeshProUGUI lhs;
    public TextMeshProUGUI rhs;
    public TextMeshProUGUI operation;

    public TextMeshProUGUI cA;
    public TextMeshProUGUI cB;
    public TextMeshProUGUI cC;
    public TextMeshProUGUI cD;

    public Button bA;
    public Button bB;
    public Button bC;
    public Button bD;

    public int correctAnswerIndex;

    public void SetupUIManager(float _lhs, float _rhs, string _operation,
            float correctAnswer, float a, float b, float c, float d,
            VoidCb trueAnserAction, VoidCb wrongAnswerAction) {

        SetQuestions(_lhs, _rhs, _operation);

        SetChoices(correctAnswer, a, b, c, d);

        SetButtonListener(trueAnserAction, wrongAnswerAction);
    }

    public void SetQuestions(float _lhs, float _rhs, string _operation) {
        lhs.text = _lhs.ToString();
        rhs.text = _rhs.ToString();
        operation.text = _operation;
    }

    public void SetChoices(float correctAnswer, float a, float b, float c, float d) {
        float[] choices = {a, b, c, d};
        choices.Shuffle();

        correctAnswerIndex = Random.Range(0, 4);
        choices[correctAnswerIndex] = correctAnswer;

        // floating point
        string fp = "0";
        if (operation.text == "/") {
            fp = "0.00";
        }

        cA.text = choices[0].ToString(fp);
        cB.text = choices[1].ToString(fp);
        cC.text = choices[2].ToString(fp);
        cD.text = choices[3].ToString(fp);
    }

    public void SetButtonListener(VoidCb trueAnswerAction, VoidCb wrongAnswerAction){
        Button[] buttons = {bA, bB, bC, bD};

        bA.onClick.AddListener(() => { wrongAnswerAction(); });
        bB.onClick.AddListener(() => { wrongAnswerAction(); });
        bC.onClick.AddListener(() => { wrongAnswerAction(); });
        bD.onClick.AddListener(() => { wrongAnswerAction(); });

        buttons[correctAnswerIndex].onClick.RemoveAllListeners();
        buttons[correctAnswerIndex].onClick.AddListener(() => trueAnswerAction());
    }

    public void SetNullAllListener() {
        bA.onClick.RemoveAllListeners();
        bB.onClick.RemoveAllListeners();
        bC.onClick.RemoveAllListeners();
        bD.onClick.RemoveAllListeners();
    }
}
