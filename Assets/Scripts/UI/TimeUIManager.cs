using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeUIManager : MonoBehaviour
{
    public TextMeshProUGUI minutes;
    public TextMeshProUGUI seconds;
    public TextMeshProUGUI colon;

    IEnumerator Start() {
        int min = int.Parse(minutes.text);
        int sec = int.Parse(seconds.text);

        Debug.Log($"min: {min} & sec {sec}");

        while (true) {
            yield return new WaitForSeconds(1f);

            if (sec <= 0) {
                sec = 60;
            }

            if (sec == 59) {
                SetMinute(--min);
            }

            SetSecond(sec--);

            if (min == 0 && sec <= 1) {
                break;
            }
        }

        colon.gameObject.SetActive(false);
        seconds.text = "";
        minutes.text = "BUZZER!";
        minutes.fontSize = 20;
    }

    public void SetMinute(float min) {
        minutes.text = min.ToString("00");
    }

    public void SetSecond(float sec) {
        seconds.text = sec == 60 ? "00" : sec.ToString("00");
    }
}
