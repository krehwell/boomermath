using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Bomb animation
 */
public class BombUpScale : MonoBehaviour
{
    public bool flipScale = false;
    public float maxSize = 1.3f;
    public float minSize = 1f;
    public float lerp = 0.01f;

    IEnumerator Start() {
        while (true) {
            yield return new WaitForSeconds(0.005f);
            float sx = transform.localScale.x;
            float sy = transform.localScale.y;
            float sz = transform.localScale.z;

            if (flipScale) {
                transform.localScale = new Vector3(sx-lerp, sy-lerp, sz-lerp);
            } else {
                transform.localScale = new Vector3(sx+lerp, sy+lerp, sz+lerp);
            }

            if (sx >= maxSize) {
                flipScale = true;
            } else if (sx <= minSize) {
                flipScale = false;
            }
        }
    }
}
