using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Helper script for ease of changing border with other model
 */
public class BorderSetupManager : MonoBehaviour
{
    public GameObject borderPrefab;
    public GameObject border2ndPrefab;

    [ContextMenu("Swap all Border with new one")]
    public void SwapAllTiles() {
        foreach (Transform child in transform) {
            Transform t = child.GetComponent<BoxCollider>().transform;

            foreach (Transform tileChild in t) {
                DestroyImmediate(tileChild.gameObject);
            }

            if (child.name == "Border") {
                Instantiate(borderPrefab, t.position, Quaternion.identity, child).name = "Border";
            } else if (child.name == "Border2nd") {
                Instantiate(border2ndPrefab, t.position, Quaternion.identity, child).name = "Border2nd";
            }
        }
    }
}
