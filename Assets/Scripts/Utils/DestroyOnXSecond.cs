using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnXSecond : MonoBehaviour
{
    public int xSecond = 2;
    IEnumerator Start() {
        yield return new WaitForSeconds(xSecond);
        Destroy(gameObject);
    }
}
