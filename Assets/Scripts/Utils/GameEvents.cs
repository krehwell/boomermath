using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        if (current != null && current != this)
        {
            Destroy(this.gameObject);
        } else {
            current = this;
        }
    }

#region Tile Events
    public event Action<Transform, int> onPropsEnterTile;
    public void PropsEnterTile(Transform objT, int index) {
        if (onPropsEnterTile != null) {
            onPropsEnterTile(objT, index);
        }
    }

    public event Action<Transform, int> onPropsExitTile;
    public void PropsExitTile(Transform objT, int index) {
        if (onPropsExitTile != null) {
            onPropsExitTile(objT, index);
        }
    }
#endregion

#region Player Events
    public event Action<string> onPlayerIsMoving;
    public void PlayerIsMoving(string playerId) {
        if (onPlayerIsMoving != null) {
            onPlayerIsMoving(playerId);
        }
    }

    public event Action<string> onPlayerStopMoving;
    public void PlayerStopMoving(string playerId) {
        if (onPlayerStopMoving != null) {
            onPlayerStopMoving(playerId);
        }
    }

    public event Action<string> onPlayerHitExplotion;
    public void PlayerHitExplotion(string playerId) {
        if (onPlayerHitExplotion != null) {
            onPlayerHitExplotion(playerId);
        }
    }

    public event Action<string> onPlayerIsWinning;
    public void PlayerIsWinning(string notTheWinnerPlayerId) {
        if (onPlayerIsWinning != null) {
            onPlayerIsWinning(notTheWinnerPlayerId);
        }
    }
#endregion
    public event Action<Transform> onDistructableWallHitExplotion;
    public void DistructableWallHitExplotion(Transform t) {
        if (onDistructableWallHitExplotion != null) {
            onDistructableWallHitExplotion(t);
        }
    }

#region Gameplay Events
    public event Action onGameTimeout;
    public void GameTimeout() {
        if (onGameTimeout != null) {
            onGameTimeout();
        }
    }
#endregion
}
