using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileUIActivation : MonoBehaviour
{
    public Player player;
    public GameObject UI;

    void Start() {
        if (player.pv.IsMine &&
                (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
                ) {
            UI.SetActive(true);
        }
    }
}
