using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This script purpose is just to set automatic value to each tile/block
 * cause I am too lazy to set them manually
 */
public class TileSetupManager : MonoBehaviour
{
    public GameObject tilePrefab;
    public GameObject tile2ndPrefab;

    [ContextMenu("Set All Tiles Id")]
    public void SetAllTilesId() {
        int i = 0;
        foreach (Transform child in transform) {
            if (child.GetComponent<Tile>()) {
                child.GetComponent<Tile>().id = i;
                i++;
            }
        }
    }

    [ContextMenu("Set All Tiles aboveIsEmtpy to True")]
    public void SetAllTilesAboveToTrue() {
        foreach (Transform child in transform) {
            if (child.GetComponent<Tile>()) {
                child.GetComponent<Tile>().SetAboveMeIsEmpty(true);
            }
        }
    }

    [ContextMenu("Swap All Tiles With The New One")]
    public void SwapAllTiles() {
        foreach (Transform child in transform) {
            Transform t = child.GetComponent<Tile>().transform;

            foreach (Transform tileChild in t) {
                DestroyImmediate(tileChild.gameObject);
            }

            if (child.name == "Tile") {
                Instantiate(tilePrefab, t.position, Quaternion.identity, child).name = "Tile Block";
            } else if (child.name == "Tile2nd") {
                Instantiate(tile2ndPrefab, t.position, Quaternion.identity, child).name = "Tile2nd Block";
            }
        }
    }
}
