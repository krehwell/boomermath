using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Wall is undisctrubtable object/props
 */
public class Wall : MonoBehaviour
{
    /**
     * Check to let tile know it has wall on top then notify it
     */
    void OnTriggerEnter(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsEnterTile(this.transform, tile.id);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Tile tile = other.GetComponent<Tile>(); // tile on top of me
        if (tile) {
            GameEvents.current.PropsExitTile(this.transform, tile.id);
        }
    }
}
