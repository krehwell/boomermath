# Boomermath
Boomermath. Math to Educate Boomer.  
[Download for Android (.apk)](https://cdn.glitch.com/1eb12dc0-5065-4999-9d6c-12563e8dc142%2FboomermathFinalize.apk?v=1626233886419)

# Gameplay
[[Teaser](https://youtu.be/PuaxwP5XdAw?t=19)] - [[Explained](https://youtu.be/BlsApv42A04?t=41)]

## How to Run
- Clone repo: `git clone "https://gitlab.com/krehwell/Boomermath.git"` _(it's better to use ssh but whatever)_
- Add Existing Project in Unity -> Target This Repo.

## How to contribute
- `cd Boomermath`
- `git checkout -b branchname`
- start developing, after finished...
- `git add .`
- `git commit -m "featurename added"`
- `git push origin branchname`

## Caveat
- Make sure to always keep upated with master:
  - `git checkout master` -> `git pull origin master` -> `git checkout branchname` -> `git merge master`
- Make sure remote is this repo:
  - check remote: `git remote -v`, incase empty...
  - add remote: `git remote add origin "https://gitlab.com/krehwell/boomermath.git"`

## Dependencies
- Unity 2020.3.9f1 (lts)
- 2D Tilemap Editor (Window > Package Manager > Search for 2D tilemap Editor)
- 2D-Extras <del>(Window > Package Manager > Click "+" Icon on Left Top > Add Package From Disk > Select "Boomermath/Assets Dev Package/2d-extras/package.json")</del> _this should have been auto import now_

## Project Structure
```
- Assets
  |- Images      # UI images
  |- Objects
    |- Material  # material for 3d object
    |- ...       # just bundle all 3d objects here
  |- Scene       # main menu scene, game main scene, etc.
    |- Etcene    # scene for testing or any unrelated things
  |- Scripts
    |- Interfaces     # objects wrapper
    |- Utils     # helper functions/classes
    |- ...       # the rest just bundle all script in here for ease of access
  |- ...
```
